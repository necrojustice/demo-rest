<?php

namespace James\GameBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use James\GameBundle\Entity\Game;

class GameController extends Controller
{
    /**
     * @Route("/game/{id}", name="get_game")
     * @Method("GET")
     */
    public function getGameAction(Request $request, $id)
    {
        $em = $this->getDoctrine();
    	$game = $em->getRepository('GameBundle:Game')->findOneById($id);

    	return new JsonResponse(
			Game::toSerialize($game),
			Response::HTTP_OK,
            array('Access-Control-Allow-Origin' => '*')
		);
    }

    /**
     * @Route("/games", name="get_all_games")
     * @Method("GET")
     */
    public function getAllGamesAction(Request $request)
    {	
    	$em = $this->getDoctrine();
    	$games = $em->getRepository('GameBundle:Game')->findAll();

    	return new JsonResponse(
    		array('Games' => Game::toArraySerialize($games)), 
    		Response::HTTP_OK, 
            array('Access-Control-Allow-Origin' => '*')
    	);
    }

    /**
     * @Route("/games/search", name="search_games")
     * @Method("POST")
     */
    public function searchAction(Request $request)
    {	
    	$content = $request->getContent();

    	$searchTerms = null;

    	if(!empty($content)){
    		$searchTerms = json_decode($content, true);
    	}

    	$em = $this->getDoctrine();

    	$users = $em->getRepository('GameBundle:Game')->search($searchTerms);
    	
    	//Add Pagination if needed

    	return new JsonResponse(
    		array('Games' => Game::toArraySerialize($games)), 
    		Response::HTTP_OK, 
            array('Access-Control-Allow-Origin' => '*')
    	);
    }
}
