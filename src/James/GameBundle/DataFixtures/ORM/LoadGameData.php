<?php

namespace James\GameBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use James\GameBundle\Entity\Game;

class LoadGameData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $platformPC = $manager->getRepository('GameBundle:Platform')->findOneByName('PC');
        $platformXbone = $manager->getRepository('GameBundle:Platform')->findOneByName('XBONE');
        $platformPS4 = $manager->getRepository('GameBundle:Platform')->findOneByName('PS4');
        $platformWiiU = $manager->getRepository('GameBundle:Platform')->findOneByName('WiiU');

        $items = array(
                        array(
                                'name' => 'Saints Row IV',
                                'releaseDate' => new \DateTime('2015-01-20'),
                                'publisher' => 'Deep Silver',
                                'thumbnail' => 'saints4xbone.jpg',
                                'background' => 'saints4xbonebg.jpg',
                                'platform' => $platformXbone,
                            ),
                        array(
                                'name' => 'Resident Evil: Remastered',
                                'releaseDate' => new \DateTime('2015-01-20'),
                                'publisher' => 'Capcom',
                                'thumbnail' => 'rexbone.jpg',
                                'background' => 'rexbonebg.jpg',
                                'platform' => $platformXbone,
                            ),
                        array(
                                'name' => 'Bloodborne',
                                'releaseDate' => new \DateTime('2015-03-24'),
                                'publisher' => 'SCEA',
                                'thumbnail' => 'bloodborneps4.jpg',
                                'background' => 'bloodborneps4bg.jpg',
                                'platform' => $platformPS4,
                            ),
                        array(
                                'name' => 'Grim Fandango Remastered',
                                'releaseDate' => new \DateTime('2015-01-27'),
                                'publisher' => 'Double Fine',
                                'thumbnail' => 'grimps4.jpg',
                                'background' => 'grimps4bg.jpg',
                                'platform' => $platformPS4,
                            ),
                        array(
                                'name' => 'Evolve',
                                'releaseDate' => new \DateTime('2015-02-10'),
                                'publisher' => '2K Games',
                                'thumbnail' => 'evolveps4.jpg',
                                'background' => 'evolveps4bg.png',
                                'platform' => $platformPS4,
                            ),
                        array(
                                'name' => 'Battlefield: Hardline',
                                'releaseDate' => new \DateTime('2015-03-21'),
                                'publisher' => 'Electronic Arts',
                                'thumbnail' => 'hardlineps4.jpg',
                                'background' => 'hardlineps4bg.jpg',
                                'platform' => $platformPS4,
                            ),
                        array(
                                'name' => 'Borderlands: The Handsome Collection',
                                'releaseDate' => new \DateTime('2015-03-24'),
                                'publisher' => '2K Games',
                                'thumbnail' => 'borderlandsxbone.jpg',
                                'background' => 'borderlandsxbonebg.jpg',
                                'platform' => $platformXbone,
                            ),
                        array(
                                'name' => 'Grand Theft Auto V',
                                'releaseDate' => new \DateTime('2014-11-18'),
                                'publisher' => 'Rockstar',
                                'thumbnail' => 'gtavps4.jpg',
                                'background' => 'gtavps4bg.jpg',
                                'platform' => $platformPS4,
                            ),
                        array(
                                'name' => 'The Last of Us Remastered',
                                'releaseDate' => new \DateTime('2014-07-29'),
                                'publisher' => 'SCEA',
                                'thumbnail' => 'lastofusps4.jpg',
                                'background' => 'lastofusps4bg.jpg',
                                'platform' => $platformPS4,
                            ),
                        array(
                                'name' => 'Dragon Age: Inquisition',
                                'releaseDate' => new \DateTime('2014-11-18'),
                                'publisher' => 'Electronic Arts',
                                'thumbnail' => 'dragonageps4.jpg',
                                'background' => 'dragonageps4bg.jpg',
                                'platform' => $platformPS4,
                            ),
                    );

        foreach($items as $item){
            $game = new Game();
            $game->setName($item['name']);
            $game->setReleaseDate($item['releaseDate']);
            $game->setPublisher($item['publisher']);
            $game->setThumbnail($item['thumbnail']);
            $game->addPlatform($item['platform']);
            $game->setBackground($item['background']);

            $manager->persist($game);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}