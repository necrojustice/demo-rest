<?php

namespace James\GameBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use James\GameBundle\Entity\Platform;

class LoadPlatformData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $items = array(
                        array(
                                'name' => 'PC'
                            ),
                        array(
                                'name' => 'XBONE'
                            ),
                        array(
                                'name' => 'PS4'
                            ),
                        array(
                                'name' => 'WiiU'
                            )
                    );

        foreach($items as $item){
            $platform = new Platform();
            $platform->setName($item['name']);
            $manager->persist($platform);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}