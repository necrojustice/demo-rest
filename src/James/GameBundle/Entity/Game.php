<?php

namespace James\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Game
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="releaseDate", type="datetime")
     */
    protected $releaseDate;

    /**
     * @var string
     *
     * @ORM\Column(name="publisher", type="string", length=255)
     */
    protected $publisher;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255)
     */
    protected $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="background", type="string", length=255)
     */
    protected $background;

    /**
     * @ORM\ManyToMany(targetEntity="\James\GameBundle\Entity\Platform", inversedBy="games")
     **/
    protected $platforms;

    public function __construct() {
        $this->platforms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set releaseDate
     *
     * @param \DateTime $releaseDate
     * @return Game
     */
    public function setReleaseDate($releaseDate)
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get releaseDate
     *
     * @return \DateTime 
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     * @return Game
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return string 
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Game
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set background
     *
     * @param string $background
     * @return Game
     */
    public function setBackground($background)
    {
        $this->background = $background;

        return $this;
    }

    /**
     * Get background
     *
     * @return string 
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * Add platform
     *
     * @param \James\GameBundle\Entity\Platform $platform
     * @return Section
     */
    public function addPlatform(\James\GameBundle\Entity\Platform $platform)
    {
        $this->platforms[] = $platform;
    }

    /**
     * Remove platform
     *
     * @param \James\GameBundle\Entity\Platform $platform
     */
    public function removePlatform(\James\GameBundle\Entity\Platform $platform)
    {
        $this->comments->removeElement($platform);
    }

    /**
     * Get platforms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlatforms()
    {
        return $this->platforms;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function toJson()
    {
        return json_encode($this->toSerialize($this));
    }

    public static function toSerialize($game)
    {
        $platformArray = array();

        foreach($game->getPlatforms() as $platform){
            $platformArray[] = array(
                                        $platform->toSerialize($platform)
                                    );
        }

        return array(
                    'id' => $game->getId(),
                    'name' => $game->getName(),
                    'publisher' => $game->getPublisher(),
                    'releaseDate' => $game->getReleaseDate(),
                    'thumbnail' => $game->getThumbnail(),
                    'platforms' => $platformArray,
                    'background' => $game->getBackground()
                    );
    }

    public static function toArraySerialize($itemArray)
    {
        $items = array();

        foreach($itemArray as $item){
            array_push($items, Game::toSerialize($item));
        }

        return $items;
    }
}
