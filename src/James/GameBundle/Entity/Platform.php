<?php

namespace James\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Platform
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Platform
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="\James\GameBundle\Entity\Game", mappedBy="platforms")
     **/
    protected $games;

    public function __construct() {
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add game
     *
     * @param \James\GameBundle\Entity\Game $game
     * @return Section
     */
    public function addGame(\James\GameBundle\Entity\Game $game)
    {
        $this->platforms[] = $game;
    }

    /**
     * Remove game
     *
     * @param \James\GameBundle\Entity\Game $game
     */
    public function removeGame(\James\GameBundle\Entity\Game $game)
    {
        $this->comments->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGames()
    {
        return $this->games;
    }

    public function toJson()
    {
        return json_encode($this->toSerialize($this));
    }

    public static function toSerialize($platform)
    {
        return array(
                    'id' => $platform->getId(),
                    'name' => $platform->getName()
                    );
    }

    public static function toArraySerialize($itemArray)
    {
        $items = array();

        foreach($itemArray as $item){
            array_push($items, Platform::toSerialize($item));
        }

        return $items;
    }
}
